import React, {Component} from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';

import Login from './Login';
import Biodata from './Biodata';
export default class App extends Component {
  render(){
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor='#FFA07A'
          barstyle='light-content'
          />
          <Biodata />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FA8072',
    alignItems: 'center',
    justifyContent: 'center'
  }
});