//Looping While
console.log('Looping Pertama')
var loop = 2;
while (loop <20) {
    if(loop %2==0){
        console.log(loop + '- I LOVE CODING');
    }
    loop++
}
console.log('\n');
console.log('Looping Kedua')
while (loop >=2) {
    if(loop %2==0){
        console.log(loop + '- I will become fullstack javascript developer');
    }
    loop--
}

console.log('\n');
//Looping For
for (var i=1;i<=20;i++){
    if (i%2==0){
        console.log(i+ "- Informatika")
    }else{
        console.log(i+ "- Teknik")
        console.log(i+ "- I LOVE CODING")
    }
}


//Membuat Persegi Panjang
var PersegiPanjang = ' ';

for(var a = 0; a<4; a++){
    for(var z =0; z<8; z++){
        PersegiPanjang +='#';
    }
    PersegiPanjang +='\n';
}
console.log(PersegiPanjang);


//Membuat Tangga
var b =' ';
for ( a = 1; a<= 7; a++){
    for( j = 1; j <= a; j++){
        b += '#';
    }
    b += '\n';
}
console.log(b);

//Membuat Papan Catur
for (var angka =1; angka <=4; angka++){
    console.log('# # # # #');
    console.log('# # # # #');
}