var word = 'JavaScript';
var second = 'is';
var third = 'awesome';
var fourth = 'and';
var fifth = 'I';
var sixth = 'love';
var seventh = 'it!';
console.log(word,second,third,fifth,sixth,seventh);


var sentence = "I am going to be React Native Developer";
var FirstWord = sentence[0];
var SecondWord = sentence[2] + sentence[3];
var ThirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var FourthWord = sentence[11] + sentence[12];
var FifthWord = sentence[14] + sentence[15];
var SixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var SeventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var EighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];
console.log('First word: ' + FirstWord);
console.log('Second word: ' + SecondWord);
console.log('Third word: ' + ThirdWord);
console.log('Fourth word: ' + FourthWord);
console.log('Fifth word: ' + FifthWord);
console.log('Sixth word: ' + SixthWord);
console.log('Seventh word: ' + SeventhWord);
console.log('Eighth word: ' + EighthWord);

var sentence2 = 'wow JavaScript is so cool';
var exampleFirstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2 = sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);
console.log('First Word: ' + exampleFirstWord2);
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);

var sentence3 = 'wow JavaScript is so cool';
var exampleFirstWord3 = sentence3.substring(0, 3);
var SecondWord3 = sentence3.substring(4, 14);
var ThirdWord3 = sentence3.substring(15, 17);
var FourthWord3 = sentence3.substring(18, 20);
var FifthWord3 = sentence3.substring(21, 25); 
var FirstWordLength = exampleFirstWord3.length
var SecondWordLength = SecondWord3.length
var ThirdWordLength = ThirdWord3.length
var FourthWordLength = FourthWord3.length
var FifthWordLength = FifthWord3.length

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' +
FirstWordLength);
console.log('Second Word: ' + SecondWord3 + ', with length: ' +
SecondWordLength);
console.log('Third Word: ' + ThirdWord3 + ', with length: ' +
ThirdWordLength);
console.log('Fourth Word: ' + FourthWord3 + ', with length: ' +
FourthWordLength);
console.log('Fifth Word: ' + FifthWord3 + ', with length: ' +
FifthWordLength); 